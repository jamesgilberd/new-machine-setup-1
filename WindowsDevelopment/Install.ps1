#requires -runasadministrator
#requires -version 5.1
#requires -psedition Desktop

$ErrorActionPreference = 'Stop'

function CheckForWindowsUpdatesThatNeedInstalling
{
    $updateSession = New-Object -ComObject Microsoft.Update.Session
    $updateSearcher = $updateSession.CreateupdateSearcher()
    $updates = @($updateSearcher.Search("IsHidden=0 and IsInstalled=0").Updates)
    $outstandingUpdates = $updates | Select-Object -Expand Title | Where-Object { $_ -notmatch 'Preview' }
    if($outstandingUpdates) {
        "The following updates need to be installed:", $outstandingUpdates | Out-String | Write-Warning
        if($env:APPVEYOR -eq 'True') {
            Write-Warning "Allowing outstanding Windows updates on Appveyor CI server."
            return
        }
        throw "This script assumes you are running the latest version of Windows 10, so you need to install Windows updates."
    }
}

function EnableDeveloperMode
{
    reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\AppModelUnlock" /t REG_DWORD /f /v "AllowDevelopmentWithoutDevLicense" /d "1"
    if(!$?) {
        throw "Failed to enable developer mode."
    }
}

function EnableVirtualTerminalEscapeSequences
{
    Set-ItemProperty HKCU:\Console VirtualTerminalLevel -Type DWORD 1
}

function UseUtcInRTC
{
    Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\TimeZoneInformation" -Name "RealTimeIsUniversal" -Type DWord -Value 1
}

function DisableInternetExplorerAutomaticProxyDetection
{
    Set-ItemProperty 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings' -Name ProxySettingsPerUser -Value 1
    $defaultSettings = [byte[]](70, 0, 0, 0, 220, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    Set-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Connections' -Name DefaultConnectionSettings -Value $defaultSettings
    Set-ItemProperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -Name AutoDetect -Value 0
}

function ExcludeDataFromRealtimeVirusScanning
{
    if((gp 'HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender\' DisableAntiSpyware -ea 0 | select -expand DisableAntiSpyware) -eq 1) { return }

    Add-MpPreference -ExclusionPath D:\
}

function InstallVisualStudio()
{
    $upgrader = "C:\Program Files (x86)\Microsoft Visual Studio\Installer\vs_installer.exe"
    $vs2019Path = "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional"
    if((Test-Path $upgrader) -and (Test-Path $vs2019Path)) {
        $upgraderProcess = Start-Process -PassThru -Wait -FilePath $upgrader -ArgumentList 'update', '--passive', '--norestart', '--installpath', "`"$vs2019Path`""
        if($upgraderProcess.ExitCode -notin (0, 1 <# VS installer has started returning 1 when there are no updates #>)) {
            throw "Upgrade of Visual Studio failed with exit code $($upgraderProcess.ExitCode)."
        }
        return
    }

    $installer = Join-Path $env:Temp VisualStudioSetup.exe
    Invoke-WebRequest "https://download.visualstudio.microsoft.com/download/pr/321da3ad-5224-41dc-80e8-dd7610460f41/f75bf30b88b8218ed3e63b0d7ddf06d8/vs_professional.exe" -OutFile $installer
    $components =
        "Microsoft.VisualStudio.Component.CoreEditor",
        "Microsoft.VisualStudio.Workload.Data",
        "Microsoft.VisualStudio.Workload.ManagedDesktop",
        "Microsoft.VisualStudio.Workload.NativeDesktop",
        "Microsoft.VisualStudio.Workload.NetCoreTools",
        "Microsoft.VisualStudio.Workload.NetWeb",
        "Microsoft.Net.Component.4.7.SDK",
        "Microsoft.Net.Component.4.7.TargetingPack",
        "Microsoft.Net.Component.4.7.1.SDK",
        "Microsoft.Net.Component.4.7.1.TargetingPack",
        "Microsoft.Net.Component.4.7.2.SDK",
        "Microsoft.Net.Component.4.7.2.TargetingPack"
    $installerArguments = "--wait", "--norestart", "--passive", "--includeRecommended"
    $components | % {
        $installerArguments += "--add"
        $installerArguments += $_
    }
    $installerProcess = Start-Process -PassThru -Wait -FilePath $installer -ArgumentList $installerArguments
    if($installerProcess.ExitCode -ne 0) {
        throw "Installation of Visual Studio failed with exit code $($installerProcess.ExitCode)."
    }
}

function KillGitProcessThatStopsUpdatesFromWorking()
{
    Get-Process -ea 0 ssh-agent | Stop-Process -Force
}

function SetExecutionPolicy()
{
    Get-ExecutionPolicy -List
    Set-ExecutionPolicy -Scope Process RemoteSigned
    Set-ExecutionPolicy -Scope LocalMachine RemoteSigned
    Get-ExecutionPolicy -List
}

function ConfigureWindowsUpdateToDownloadFromLocalPcs()
{
    $path = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeliveryOptimization\Config"
    if (-not (Test-Path $path))
    {
        New-Item -Force $path | Out-Null
    }
    Set-ItemProperty -Path $path -Name DODownloadMode -Type DWord -Value 1
}

function EnableLongPaths()
{
    Set-ItemProperty 'HKLM:\System\CurrentControlSet\Control\FileSystem' -Name 'LongPathsEnabled' -value 1
}

function EnsureChocolateyInstalled()
{
    if(!(Test-Path $Profile)) { New-Item -Force -ItemType file $Profile | Out-Null } # if $profile doesn't exist, Chocolatey won't load its module
    if(!(gcm cinst -ErrorAction SilentlyContinue)) {
        Write-Host "Installing Chocolatey..."
        iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
        $env:path += "$($env:SYSTEMDRIVE)\chocolatey\bin"
        . $Profile
        Write-Host "Chocolatey installed."
    }

    Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
    choco feature enable -name=exitOnRebootDetected
}

function SetHighPerformancePowerProfile()
{
    if((Get-Service power | % Status) -ne 'Running') {
        Write-Warning "The power service is not running, so we won't attempt to configure the power plan."
        return
    }

    powercfg -setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c
    if(!$?) {
        throw "Failed to set power plan to 'High performance'."
    }
}

function InstallFromChocolatey()
{
    $tools =
        '7zip',
        '7zip.commandline',
        'azure-cli',
        'azure-functions-core-tools-3 /x64',
        'bat',
        'beyondcompare',
        'cmake',   # for Vim/YouCompleteme
        'cpu-z',
        'crystaldiskmark',
        'cutter',
        'delta',
        'dive',
        'dependencywalker',
        'dotnetcore-sdk',
        'editorconfig.core',
        'ffmpeg',
        'fiddler',
        'filezilla',
        'Firefox',
        'fzf',
        'Ghostscript',
        'gimp',
        'git /GitAndUnixToolsOnPath /NoAutoCrlf',
        #'global',   # For Pascal autocomplete in VS Code - and maybe others (GNU Global); currently broken
        'glow',
        'GoogleChrome',
        'graphviz.portable',
        'hxd',
        'html-tidy',
        'imagemagick',
        'jetbrainstoolbox',
        'jq',
        'keepass',
        'kitty.portable',
        'kubernetes-cli',
        'kubernetes-helm',
        'kubernetes-helmfile',
        'libreoffice-fresh',
        'linqpad5.anycpu.install',
        'lua53',    # for Vim
        'logparser',
        'microsoft-windows-terminal',
        'r.project',
        'miktex',
        'mitmproxy',
        'mremoteng',
        'nmap',
        'winpcap',  # to make sure that we have the latest version installed for nmap
        'nodejs-lts',
        'nuget.commandline',
        'openjdk',
        'pandoc',
        'perfview',
        'python3',
        'poshgit',
        'postman',
        'r.studio',
        'ripgrep-all',
        'rsat',
        'rufus',
        'screentogif',
        'slack',
        'sqlgrep',
        'sysinternals',
        'terraform',
        'treesizefree',
        'universal-ctags',    # for Vim - Vista plugin
        'vim-tux /InstallPopUp /RestartExplorer',
        'visualstudiocode',
        'wireguard',
        'wireshark',
        'wsl',
        'wsl2',
        'xsltproc',
        'xsv'

    $chocPath = Join-Path $env:ChocolateyInstall 'choco.exe'
    $alreadyInstalled = & $chocpath list --local-only | ? { ($_ -split ' ').length -eq 2 } | % { ($_ -split ' ')[0] }
    $newTools = $tools | ? { $alreadyInstalled -notcontains ($_ -split ' ', 2)[0] }

    function RebootIfRequired($chocolateyExitCode) {
        if($chocolateyExitCode -ne 350) {
            return
        }
        Write-Warning "Your computer needs to reboot to complete a previous software installation. Please reboot and restart this script."
        Start-Sleep -Seconds 2147483
        Restart-Computer
    }

    Write-Host "Updating all currently installed components..."
    Update-SessionEnvironment
    & $chocPath upgrade all -y
    RebootIfRequired $LASTEXITCODE
    Update-SessionEnvironment

    Write-Host "Installing all new components..."
    $newTools | % {
        $package, $packageParameters = $_ -split ' ', 2

        if($packageParameters) {
            & $chocPath install -y $package -params $packageParameters
        }
        else {
            & $chocPath install -y $package
        }

        RebootIfRequired $LASTEXITCODE
        Update-SessionEnvironment
    }
}

function UpgradeGlobalNpmPackages
{
    npm upgrade -g
}

function InstallPackagesUsingPythonPip
{
    python -m pip install -U pip httpie mssql-cli
}

function InstallHelmPlugins
{
    helm plugin install https://github.com/databus23/helm-diff
}

function InstallPackagesUsingWindowsPackageManagement()
{
    Get-PackageSource PSGallery | Set-PackageSource -Trusted
    Install-PackageProvider -Name NuGet -Force

    if(Get-Package -ea 0 PSVSEnv) { Uninstall-Package PSVSEnv }
    Install-Package -AllowClobber -Source psgallery PSVSEnv
}

function UpdatePowerShellProfile()
{
    $profileComment = "# Installed by Bart's BitBucket script"

    if((cat $profile) -contains $profileComment) {
        return
    }

    $commands = @($profileComment, "if(gmo -list Hyper-V) { ipmo Hyper-V }", "if(!(gcm -ea 0 msbuild)) { vs2019 }", $profileComment) -join "`r`n"

    Add-Content -Path $profile -Value "`r`n`r`n$commands`r`n"
}

function UpdatePowerShellHelp()
{
    Update-Help -Force -ErrorAction Continue
}

function ConfigureGit()
{
    $beyondCompareExePath = "C:/Program Files/Beyond Compare 4/bcomp.exe"

    git config --global rebase.autoSquash true
    git config --global core.autocrlf true
    git config --global core.longpaths true
    git config --global core.symlinks true
    git config --global core.whitespace cr-at-eol
    git config --global difftool.prompt false
    git config --global mergetool.keepbackup false
    git config --global mergetool.prompt false
    git config --global receive.denynonfastforwards true    # mostly suitable for servers
    git config --global core.logallrefupdates true          # mostly suitable for servers
    git config --global diff.colorMoved zebra
    git config --global diff.algorithm histogram
    git config --global diff.renameLimit 10000
    git config --global http.sslbackend schannel
    git config --global core.pager 'delta --dark --color-only'

    git config --global diff.tool bc
    git config --global difftool.bc.path $beyondCompareExePath
    git config --global mergetool.bc.path $beyondCompareExePath
    git config --global merge.tool bc-custom
    git config --global mergetool.bc-custom.cmd ('\"' + $beyondCompareExePath + '\" \"$LOCAL\" \"$REMOTE\" \"$BASE\" -fv=\"Text Merge\" -mergeoutput=\"$MERGED\" -nobackups')

    git config --global difftool.rider.cmd '\"C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe\" -NoProfile -Command \"start -Wait (ls -r ~/AppData/Local/JetBrains/Toolbox/apps/Rider -fi rider64.exe | sort FullName | select -last 1 -expand FullName) -ArgumentList ''diff'', ''\\\"$LOCAL\\\"'', ''\\\"$REMOTE\\\"''\"'
    git config --global mergetool.rider.cmd '\"C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe\" -NoProfile -Command \"start -Wait (ls -r ~/AppData/Local/JetBrains/Toolbox/apps/Rider -fi rider64.exe | sort FullName | select -last 1 -expand FullName) -ArgumentList ''merge'', ''\\\"$LOCAL\\\"'', ''\\\"$REMOTE\\\"'', ''\\\"$BASE\\\"'', ''\\\"$MERGED\\\"''\"'

    git config --global core.excludesfile '~/.gitignore'
    Set-Content -Encoding UTF8 -LiteralPath "~/.gitignore" -Value ".idea/`n.vscode/`n.vs/`n*.ncrunchsolution`n*.ncrunchsolution.user`n_NCrunch_*`n*.sln.DotSettings.user"
}

function ConfigureDocumentGeneration()
{
    python -m pip install --upgrade pip pygments

    $path = [Environment]::GetEnvironmentVariable('PATH', 'Machine')
    $scriptPath = Join-Path ([System.IO.Path]::GetDirectoryName((gcm python | select -expand Source))) 'Scripts'
    if($path -notlike ("*" + $scriptPath + "*")) {
        [Environment]::SetEnvironmentVariable('PATH', "$PATH;$scriptPath", 'Machine')
    }

    Update-SessionEnvironment
}

function InstallVim()
{
    npm install -g js-beautify typescript

    if(!(Test-Path ~\vimfiles)) {
        pushd ~
        try {
            git clone -q https://bartj@bitbucket.org/bartj/vim.git vimfiles
            if($LASTEXITCODE -ne 0) {
                throw "Failed to clone repo."
            }
            cmd /c mklink _vimrc vimfiles\vimrc
            cmd /c mklink _gvimrc vimfiles\gvimrc
        }
        finally {
            popd
        }
    }

    Write-Host "Ensuring cmake is on the path for YouCompleteMe."
    if($env:path -notmatch 'cmake') {
        $env:path+=';C:\Program Files\CMake\bin'
        Write-Host -ForegroundColor Green "Added CMake to the  path for the current process."
    }

    $vimFilesPath = Resolve-Path ~/vimfiles
    Write-Host "vimfiles at $vimFilesPath."
    $ErrorActionPreference = 'Continue'
    git -C $vimFilesPath pull -q --autostash --rebase
    if($LASTEXITCODE -ne 0) {
        throw "Failed to update repo."
    }
    $ErrorActionPreference = 'Stop'
    $vim = ls 'C:\Program Files\vim' -r -fi gvim.exe | sort DirectoryName | select -last 1 -expand FullName
    Write-Host "Using vim at $vim."
    start $vim '-c ":PlugInstall" -c ":PlugUpdate" -c ":PlugClean!" -c "qa"' -Wait

    Write-Host "Installing code completion plugins..."
    start $vim '-c "CocInstall -sync coc-marketplace coc-json coc-html coc-powershell coc-vimlsp coc-python coc-tsserver coc-xml coc-sql coc-omnisharp coc-markdownlint" -c "qa"' -Wait
    Write-Host "Plugins installed."
}

function UpgradeMiktex
{
    $installDir = "C:\Program Files\miktex*"
    if(!(Test-Path $installDir)) {
        Write-Warning "Could not find MiKTeX installation directory; did it install correctly?"
        return
    }
    $mpmExe = ls $installDir -r -fi mpm.exe | select -expand FullName | sort | select -Last 1

    if (-not $mpmExe) {
        Write-Error "mpm.exe not found. Miktex probably wasn't installed correctly. Fix this and try again."
    }

    Write-Host "Upgrading with --admin."
    & $mpmExe --verbose --update --admin
    Write-Host "Upgrading without --admin."
    & $mpmExe --verbose --update
}

function ExposeLogParserOnPath
{
    # LogParser 2.2 package does not correctly link the LogParser.exe
    $LogParserLinkPath = (Join-Path $env:ChocolateyInstall "bin\LogParser.exe")
    if(-not (Test-Path $LogParserLinkPath)) {
        New-Item -ItemType SymbolicLink -Path $LogParserLinkPath -Target "C:\Program Files (x86)\Log Parser 2.2\LogParser.exe"
    }
}

function InstallPowerShellModules
{
    Install-Module Z -AllowClobber
    Install-Module SqlServer -AllowClobber
    Update-Module Z, SqlServer
}

function ConfigureVisualStudioCode
{
    code --install-extension EditorConfig.EditorConfig
}

function DisableSomeTelemetry
{
    [Environment]::SetEnvironmentVariable('VSCMD_SKIP_SENDTELEMETRY', "true", 'Machine')   #in response to https://developercommunity.visualstudio.com/content/problem/694847/vsdevcmdbat-developer-prompt-causes-the-input-line.html
}

'CheckForWindowsUpdatesThatNeedInstalling',
'EnableDeveloperMode',
'EnableVirtualTerminalEscapeSequences',
'UseUtcInRTC',
'DisableInternetExplorerAutomaticProxyDetection',
'ExcludeDataFromRealtimeVirusScanning',
'InstallVisualStudio',
'KillGitProcessThatStopsUpdatesFromWorking',
'SetExecutionPolicy',
'ConfigureWindowsUpdateToDownloadFromLocalPcs',
'EnableLongPaths',
'SetHighPerformancePowerProfile',
'EnsureChocolateyInstalled',
'InstallFromChocolatey',
'UpgradeGlobalNpmPackages',
'InstallPackagesUsingPythonPip',
'InstallHelmPlugins',
'InstallPackagesUsingWindowsPackageManagement',
'UpdatePowerShellProfile',
'UpdatePowerShellHelp',
'ConfigureGit',
'ConfigureDocumentGeneration',
'InstallVim',
'ExposeLogParserOnPath',
'InstallPowerShellModules',
'ConfigureVisualStudioCode',
'DisableSomeTelemetry',
'UpgradeMiktex' | % {
    Write-Host -ForegroundColor Green $_
    & $_
}
Write-Host -ForegroundColor Green "Installation complete."
