[![Build status](https://ci.appveyor.com/api/projects/status/bh084kx4kb25qv3n/branch/master?svg=true)](https://ci.appveyor.com/project/nzbart/new-machine-setup)

Below are instructions for different operating systems.

Note that all scripts are designed to be re-runnable to update your system with the latest versions of the software and to incorporate any enhancements in the scripts.

## Debian development (with GUI)

First run as root:

```text
# bash <(wget -O - https://bitbucket.org/bartj/new-machine-setup/raw/master/Debian/1.install-root.sh)
```

Then as normal user:

```text
$ bash <(wget -O - https://bitbucket.org/bartj/new-machine-setup/raw/master/Debian/2.install-user.sh)
```

## Windows development

```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://bitbucket.org/bartj/new-machine-setup/raw/master/WindowsDevelopment/Install.ps1')
```

Note that the Windows installer assumes you're running as a user who can elevate to administrator, rather than a (more secure) standard user that needs to log into a different account in order to gain administrative rights. This is because I typically run my development machines as users with administrative rights sitting behind a UAC prompt for convenience.

## Editor configuration

For cross-platform work, use the `.gitattributes` and `.editorconfig` files in the root of this repository. The Git attributes file can be retroactively applied using this PowerShell:

```powershell
Set-Content -Path .gitattributes "*   text=auto" -Encoding Ascii
git read-tree --empty
git add .
git commit -m "Standardise on line endings"
```
