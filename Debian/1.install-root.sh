#!/bin/bash
set -e
set -x

#Recommended sources.list:
#deb https://deb.debian.org/debian/ testing main non-free contrib
#deb http://security.debian.org testing-security main non-free contrib
#deb https://deb.debian.org/debian/ testing-updates main non-free contrib

#Run as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

#Persist logs through reboot
mkdir -p /var/log/journal

#Show IP address, SSH keys, and other details at login prompt
mkdir -p /opt/auto-issue
cat > /opt/auto-issue/update-issue <<'EOL'
mkdir -p /etc/issue.d
echo "[1m[38;5;21mDetails current at $(date -Iseconds)[0m

[1m[38;5;124mHostname:[0m
$(hostname --fqdn | sort | uniq | sed 's/^/  /')
[1m[38;5;124mUsers:[0m
$(getent passwd | grep -v ':\(/bin/false\|/usr/sbin/nologin\|/bin/sync\)$' | cut -d : -f 1 | sort | uniq | sed 's/^/  /')
[1m[38;5;124mMemory:[0m
$(vmstat -s --unit M | egrep '(total memory|used memory)' | awk '{print $3 "\t" $1 " MiB"}' | sort | sed 's/^/  /')
[1m[38;5;124mDisk space:[0m
$(findmnt --real --output 'TARGET,AVAIL,SIZE,USE%,SOURCE' | sed 's/^/  /')
[1m[38;5;124mIP address:[0m
$(hostname -I | tr ' ' '\n' | sed '/^$/d' | sort | uniq | sed 's/^/  /')
[1m[38;5;124mDefault gateway:[0m
$(ip route show to default | cut -d ' ' -f 3 | sort | uniq | sed 's/^/  /')
[1m[38;5;124mSearch domain:[0m
$(grep -E '^(domain|search)' /etc/resolv.conf | cut -d ' ' -f 2 | sed 's/^/  /')
[1m[38;5;124mDNS server:[0m
$(grep ^nameserver /etc/resolv.conf | cut -d ' ' -f 2 | sed 's/^/  /')
[1m[38;5;124mDHCP server:[0m
$({ grep dhcp-server-identifier /var/lib/dhcp/dhclient.*leases | sed -E 's/.*dhcp-server-identifier (.*);.*/\1/p'; ls /sys/class/net | xargs -n 1 dhcpcd -U | grep dhcp_server_identifier | sed -E $'s/.*=\'(.*)\'/\\1/'; } | sort | uniq | sed 's/^/  /')
[1m[38;5;124mSSH key:[0m
$(ssh-keyscan localhost | ssh-keygen -lf - | sort | uniq | sed 's/^/  /')
" > /etc/issue.d/login-details.issue
EOL
chmod +x /opt/auto-issue/update-issue
mkdir -p /etc/systemd/system/getty@.service.d
echo "[Service]
ExecStartPre=-/bin/bash -c '/opt/auto-issue/update-issue'" > /etc/systemd/system/getty@.service.d/auto-issue.conf
systemctl daemon-reload

#Add packages
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get install -y software-properties-common
apt-add-repository non-free
apt-add-repository contrib
apt-get update
apt-get -y autoremove
apt-get -y full-upgrade

echo "Not installing these broken packages:"
echo "    bat"
echo "    conky-std"

apt-get -y install \
    apt-transport-https \
    aptitude \
    atop \
    autojump \
    bat \
    build-essential \
    ca-certificates \
    clang \
    cmake \
    command-not-found \
    cups \
    cups-browsed \
    curl \
    debian-security-support \
    debsums \
    dlocate \
    docker.io \
    editorconfig \
    etckeeper \
    ffmpeg \
    firmware-linux \
    firmware-linux-nonfree \
    fzf \
    gnupg2 \
    gimp \
    git \
    git-crypt \
    golang \
    gron \
    gscan2pdf \
    hardinfo \
    hexedit \
    highlight \
    html-xml-utils \
    htop \
    httpie \
    iftop \
    intel-microcode \
    inxi \
    iotop \
    jq \
    keepassxc \
    lshw \
    lsof \
    lua5.2 \
    mitmproxy \
    mono-devel \
    ncdu \
    nmap \
    nmon \
    node-js-beautify \
    node-typescript \
    nodejs \
    npm \
    obs-studio \
    p7zip-full \
    powerline \
    putty \
    python3-pip \
    python3-dev \
    ranger \
    remmina \
    ripgrep \
    ruby \
    ruby-dev \
    rxvt-unicode \
    saidar \
    samba \
    screenfetch \
    shellcheck \
    smbclient \
    snapd \
    stterm \
    sudo \
    testssl.sh \
    texlive \
    texlive-latex-extra \
    tidy \
    tmux \
    trash-cli \
    tree \
    vim-gtk \
    vim-nox \
    virt-manager \
    vuls \
    watchman \
    whois \
    wireshark \
    xclip \
    youtube-dl \
    zsh

# Install Microsoft .NET repo
wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
# Install VS Code repo
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
# Install software
apt-get update
apt-get install -y dotnet-sdk-5.0 azure-cli code

# Install Signal desktop
wget -O- https://updates.signal.org/desktop/apt/keys.asc | apt-key add -
echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | tee /etc/apt/sources.list.d/signal-xenial.list
apt-get update
apt-get install signal-desktop

# Update all NPM packages
npm update -g

# Install Python packages
pip3 install --upgrade sshch

# Install Ruby packages
gem install colorls
gem update colorls

# Install antibody for zsh and set up profile to load bash profile
curl -sfL git.io/antibody | sh -s - -b /usr/local/bin
echo "emulate sh -c 'source /etc/profile'" > ~/.zprofile

# Install libsecret Git credential manager
apt-get install -y libsecret-1-dev
pushd /usr/share/doc/git/contrib/credential/libsecret
make
popd

# Install rstudio
apt-get -y install libxml2-dev r-base libcurl4-openssl-dev
wget "$(curl -Ss https://www.rstudio.com/products/rstudio/download/ | hxnormalize -x | hxselect "td a[href^=\"https://download1.rstudio.org/desktop/bionic/amd64/\"][href\$=\"-amd64.deb\"]::attr(href)" -c)" -O rstudiodebian.deb
apt-get install -y ./rstudiodebian.deb
rm ./rstudiodebian.deb

# Install Microsoft fonts
pushd /usr/share/fonts
mkdir -p microsoft
cd microsoft
wget https://bitbucket.org/bartj/new-machine-setup/raw/master/Debian/fonts.7z
7za x fonts.7z -y
rm fonts.7z
popd

# Install Powerline10k font
for fonttype in Regular Bold Italic Bold%20Italic
do
    mkdir -p /usr/share/fonts/powerline10k
    wget "https://github.com/romkatv/dotfiles-public/raw/master/.local/share/fonts/NerdFonts/MesloLGS%20NF%20$fonttype.ttf" -P /usr/share/fonts/powerline10k
done

# Install Beyond Compare
#curl https://www.scootersoftware.com/RPM-GPG-KEY-scootersoftware | apt-key add -
#echo 'deb https://www.scootersoftware.com/ bcompare4 non-free' > /etc/apt/sources.list.d/scootersoftware.list
#apt-get update
#apt-get install -y bcompare

# List unmanaged packages
set +x
set +e
echo
echo '################################ package information ################################'
echo
echo '##### packages that cannot be automatically updated #####'
aptitude search '~o'
echo 'This command line may be handy:'
echo "aptitude search '~o' | grep '^i' | cut -d ' ' -f 3 | fzf -m | xargs apt-get -y remove"

echo
echo '##### packages with no security support #####'
check-support-status --no-heading

echo
echo '##### packages are missing their hash file #####'
debsums -l

echo
echo '##### potentially corrupt packages #####'
debsums -c

echo
echo '#####################################################################################'
echo
echo "Root installation successful; now run per-user installation as yourself."
