#!/bin/bash
set -e
set -x

#Install VIM
if [ -d "$HOME/.vim" ]; then
    echo "It looks like there is already a VIM installation."
else
    pushd ~
    git clone https://bartj@bitbucket.org/bartj/vim.git .vim
    ln -s .vim/vimrc .vimrc
    ln -s .vim/gvimrc .gvimrc
    popd
fi

git -C ~/.vim pull --autostash --rebase
vim -c ":PlugInstall" -c ":PlugUpdate" -c ":PlugClean!" -c "qa"

#Install zsh command-shell
if [ "$(getent passwd "$USER" | cut -d: -f7)" != "/usr/bin/zsh" ]; then
    chsh -s "$(command -v zsh)"
fi
if [ -d "$HOME/.cache/antibody" ]; then
    antibody update
fi
cat > ~/.zshrc <<'EOL'
source <(antibody init)
antibody bundle agkozak/zsh-z
antibody bundle zdharma/fast-syntax-highlighting
antibody bundle mafredri/zsh-async
antibody bundle zsh-users/zsh-autosuggestions
antibody bundle zsh-users/zsh-completions
antibody bundle zsh-users/zsh-history-substring-search
antibody bundle romkatv/powerlevel10k
antibody bundle wfxr/forgit

zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit
typeset -i updated_at=$(date +'%j' -r ~/.zcompdump 2>/dev/null || stat -f '%Sm' -t '%j' ~/.zcompdump 2>/dev/null)
if [ $(date +'%j') != $updated_at ]; then
      compinit -i
  else
        compinit -C -i
fi
zmodload -i zsh/complist
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
setopt share_history
setopt inc_append_history
setopt hist_reduce_blanks
setopt hist_ignore_all_dups
setopt interactivecomments
setopt auto_list
setopt auto_menu
setopt always_to_end
zstyle ':completion:*' menu select
zstyle ':completion:*' group-name ''
zstyle ':completion:::::' completer _expand _complete _ignored _approximate
bindkey -v

source /etc/zsh_command_not_found

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey '^[[3~' delete-char
bindkey '^[3;5~' delete-char

# z
ZSHZ_KEEP_DIRS=( /data )

# colorls
source $(dirname $(gem which colorls))/tab_complete.sh
alias lc='colorls -lA --sd --gs'

# For autojump plugin in ranger
source /usr/share/autojump/autojump.zsh

# For FZF
source /usr/share/doc/fzf/examples/key-bindings.zsh
source /usr/share/doc/fzf/examples/completion.zsh

# Default editors
export EDITOR=/usr/bin/vim
export VISUAL=/usr/bin/vim

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh

# Work around bug with autocomplete on WSL: https://github.com/zdharma/fast-syntax-highlighting/issues/146
if grep -q Microsoft /proc/version; then
    echo "Putting bug workaround in place for https://github.com/zdharma/fast-syntax-highlighting/issues/146."
    FAST_HIGHLIGHT[chroma-git]="chroma/-ogit.ch"
fi

# Configure for use with X410 on WSL
if grep -q Microsoft /proc/version; then
    export DISPLAY=127.0.0.1:0.0
fi
EOL

#Configure rxvt-unicode
cat > ~/.Xdefaults <<'EOL'
Xft.antialias:              true
Xft.rgba:                   rgb
Xft.hinting:                true
Xft.hintstyle:              hintslight

URxvt.depth:                32
URxvt.loginShell:           true
URxvt.geometry:             125x40
URxvt.allow_bold:           true
URxvt.letterSpace:          -1
URxvt.font:                 xft:MesloLGS NF:style=Regular:size=11
urxvt.boldFont:             xft:MesloLGS NF:style=Bold:size=11
URxvt.scrollStyle:          rxvt
URxvt.scrollBar:            false
URxvt.perl-ext-common:      default,matcher,fullscreen
URxvt.matcher.button:       1
URxvt.urlLauncher:          firefox
URxvt.cursorBlink:          false
URxvt.cursorColor:          #657b83
URxvt.cursorUnderline:      false
URxvt.pointerBlank:         true

URxvt*background: [90]#2B2B2B
URxvt*foreground: #DEDEDE
URxvt*colorUL: #86a2b0
! black
URxvt*color0  : #2E3436
URxvt*color8  : #555753
! red
URxvt*color1  : #CC0000
URxvt*color9  : #EF2929
! green
URxvt*color2  : #4E9A06
URxvt*color10 : #8AE234
! yellow
URxvt*color3  : #C4A000
URxvt*color11 : #FCE94F
! blue
URxvt*color4  : #3465A4
URxvt*color12 : #729FCF
! magenta
URxvt*color5  : #75507B
URxvt*color13 : #AD7FA8
! cyan
URxvt*color6  : #06989A
URxvt*color14 : #34E2E2
! white
URxvt*color7  : #D3D7CF
URxvt*color15 : #EEEEEC
EOL

# Install or update PowerShell
dotnet tool update --global PowerShell

#Configure vscode if not on WSL
grep -q Microsoft /proc/version || code --install-extension EditorConfig.EditorConfig

#Configure Powerline10k fonts
if gsettings list-keys org.gnome.Terminal.Legacy.Profile; then
    gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/ font 'MesloLGS NF 11'
    gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/ use-system-font false
else
    echo 'Unable to set Powerline fonts.'
fi

#Configure Git
git config --global rebase.autoSquash true
git config --global credential.helper /usr/share/doc/git/contrib/credential/libsecret/git-credential-libsecret
git config --global difftool.prompt false
git config --global mergetool.keepbackup false
git config --global mergetool.prompt false
git config --global diff.colorMoved zebra
git config --global diff.algorithm histogram
git config --global diff.renameLimit 10000
git config --global diff.tool bc
git config --global merge.tool bc
git config --global difftool.bc.trustExitCode true
git config --global mergetool.bc.trustExitCode true
# shellcheck disable=SC2016
git config --global difftool.rider.cmd '$(find ~/.local/share/JetBrains/Toolbox/apps/Rider -name rider.sh | sort | tail -1) diff "$LOCAL" "$REMOTE"'
# shellcheck disable=SC2016
git config --global mergetool.rider.cmd '$(find ~/.local/share/JetBrains/Toolbox/apps/Rider -name rider.sh | sort | tail -1) merge "$LOCAL" "$REMOTE" "$BASE" "$MERGED"'

# shellcheck disable=SC2088
git config --global core.excludesfile '~/.gitignore'
echo ".idea/" > ~/.gitignore

#Don't force dialogs to stick in front of the parent window
if gsettings list-keys org.gnome.mutter; then
    gsettings set org.gnome.mutter attach-modal-dialogs false
else
    echo 'Unable to set Powerline fonts.'
fi

#Disable suspend when on AC power
if gsettings list-keys org.gnome.settings-daemon; then
    gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 0
else
    echo 'Unable to disable suspend while on AC power.'
fi

#Configure TMUX
touch ~/.tmux.conf
grep -qxF 'set -g default-terminal "screen-256color"' ~/.tmux.conf || echo 'set -g default-terminal "screen-256color"' >> ~/.tmux.conf
grep -qxF 'source /usr/share/powerline/bindings/tmux/powerline.conf' ~/.tmux.conf || echo 'source /usr/share/powerline/bindings/tmux/powerline.conf' >> ~/.tmux.conf

#Install ranger plugins
wget https://raw.githubusercontent.com/fdw/ranger-autojump/master/autojump.py -P ~/.config/ranger/plugins
cat > ~/.config/ranger/rc.conf <<'EOL'
set cd_tab_case smart
set preview_images true
set vcs_aware false
set draw_borders both
EOL

echo "Per-user installation successful."
